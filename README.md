﻿Unstandard experimental
===================================
*Unstandard experimental* is a coarse library for general purpose usage aimed to be
an addition to the [D](http://dlang.org) unstandard runtime library
[Unstandard](http://denis-sh.bitbucket.org/unstandard/).

It consists of modules with (some of) the following problems:

* incomplete or inconvenient API
* bad performance
* ugly implementation
* lack of documentation

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
