﻿/**
String formatting functions.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.format;


import std.algorithm;
import std.array;
import std.ascii;
import std.range;
import std.traits;

import unstdexp.utf;


string format(Char, A...)(in Char[] fmt, auto ref A args)
in
{
	assumeFormatValid!A(fmt);
}
body
{
	import std.array : appender;

	auto w = appender!string();
	w.formattedWrite(fmt, args);
	return w.data;
}

@safe pure nothrow unittest
{
	import std.typetuple;

	alias fmt = format;

	// Escaping '*' and '^'

	assert(fmt("^*^") == "*");
	assert(fmt("^^") == "^");
	assert(fmt("**", 1, 2) == "12"); // No escaping

	// '*', Simple substitution

	static assert(!__traits(compiles, fmt("*", true)));

	foreach(T; TypeTuple!(byte, short, int, long))
		assert(fmt("* * *", cast(T) -1, cast(T) 0, cast(T) 1) == "-1 0 1");

	foreach(T; TypeTuple!(ubyte, ushort, uint, ulong))
		assert(fmt("* *", cast(T) 0, cast(T) 1) == "0 1");

	assert(fmt("***", 'a', cast(wchar)'b', cast(dchar)'c') == "abc");
	assert(fmt("***", "a", "b"w, "c"d) == "abc");

	// '^...', Formatted substitution

	// Integral types

	assert(fmt("^b ^b|^d ^d|^x ^x|^X ^X", 1U, 10U, 0, 10, 0U, 10U, 0U, 10U) == "1 1010|0 10|0 a|0 A");
	assert(fmt("^3b ^3d ^3x ^3X", 3U, 4, 10U, 11U) == "011 004 00a 00B");
	assert(fmt("^*b ^*d ^0*X",  3, 2U,  4, 5,  3, 10U) == "010 0005 0x00A");

	assert(fmt("^+d ^+d ^+d  ^+d ^+d  ^+2d", -1, 0, 1,  0U, 1U,  3) == "-1 0 +1  0 +1  +03");

	assert(fmt("^0b ^0b ^0b ^0b", 0U, 1U, 2U, 3U) == "0 1 0b10 0b11");
	assert(fmt("^0X ^0X ^0X ^0X", 0U, 9U, 10U, 16U) == "0 9 0xA 0x10");
	assert(fmt("^01b ^01x ^0*X", 0U, 0U, 4, 0U) == "0b0 0x0 0x0000");

	foreach(T; TypeTuple!(byte, short, int, long, ubyte, ushort, uint, ulong))
		assert(fmt("^*d ^*x", cast(T) 3, 5, cast(immutable T) 4, 10U) == "005 000a");

	assert(fmt("T* at ^04X get ^+d", 3, 0xF1U, 1) == "T3 at 0x00F1 get +1");

	// Formatting user types

	{
		enum E { a }
		struct S0 { }
		static assert(!__traits(compiles, fmt("*", null)));
		static assert(!__traits(compiles, fmt("*", E.a)));
		static assert(!__traits(compiles, fmt("*", S0())));
		static assert(!__traits(compiles, () @trusted { fmt("*", new Object); }));
	}
	{
		struct S
		{
			string toString()
			{ return "a"; }
		}

		class C
		{
			override string toString() const
			{ return "a"; }
		}

		assert(fmt("*", S()) == "a");
		static assert(!__traits(compiles, fmt("*", const S())));
		foreach(T; TypeTuple!(C, const C, immutable C))
			assert(fmt("*", new T) == "a");
	}
	{
		struct SNoCopy
		{
			@disable this(this);

			string toString() const
			{ return "a"; }
		}

		assert(fmt("*", SNoCopy()) == "a");
		const SNoCopy snc;
		assert(fmt("*", snc) == "a");
	}
}


void formattedWrite(Writer, Char, A...)(Writer w, in Char[] format, auto ref A args)
if(isOutputRange!(Writer, Char) && isSomeChar!Char)
in
{
	assumeFormatValid!A(format);
}
body
{
	static immutable argTypes = _getArgumentTypes!A();

	size_t usedArgs = 0;
	bool escaped = false;

	void putArg(in size_t idx, in FormatSpec spec)
	{
		foreach(const i, T; A) if(i == idx)
		{
			formatValue!Char(w, spec, args[i]);
			return;
		}
		assert(0);
	}

	size_t getArgWidth(in size_t idx) pure nothrow @nogc
	{
		foreach(const i, T; A) static if(isIntegral!T) if(i == idx)
		{
			const width = args[i];

			static if(isSigned!T)
				assert(width >= 0, "Argument dependent width can't be negative.");
			assert(width, "Argument dependent width can't be zero.");
			assert(width < 10 ^^ maxWidthDigits, "Argument dependent width is too big.");

			return cast(size_t) width;
		}
		assert(0);
	}

	auto fmt = format[];
	L: for(;;)
	{
		const(Char)[] rawChunk;
		const n = fmt._skipFmtRawChunk(escaped, rawChunk);
		put(w, rawChunk);
		final switch(n)
		{
			case 0: // not found
				if(fmt.empty)
					break L;
				break;
			case 1: // '*'
				assert(usedArgs < args.length); // More arguments needed.
				putArg(usedArgs, FormatSpec());
				++usedArgs;
				break;
			case 2: // '^'
				assert(!fmt.empty); // Start of format specifier '^' at end of string.

				if(fmt[0] == '^') // '^^'
					break;

				if(fmt[0] == '*')
				{
					assert(fmt.length > 1); // Unfinished format specifier '^*'.
					if(fmt[1] == '^') // '^*^'
						break;
				}

				escaped = false;

				const plus = fmt[0] == '+';
				if(plus)
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty); // Unexpected end of format string after format specifier plus.
				}

				const leadingZero = fmt[0] == '0';
				if(leadingZero)
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty); // Unexpected end of format string after format specifier leading zero.
					assert(fmt[0] != '0'); // Format specifier width can't start with '0'.
				}

				size_t width = 0;
				if(fmt[0] == '*') // argument dependent width
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty); // Unexpected end of format string after format specifier argument dependent width.

					assert(usedArgs < args.length); // More arguments needed.
					assert(argTypes[usedArgs] != _ArgumentType.other); // Format specifier argument dependent width can only be used with integral values.
					width = getArgWidth(usedArgs);
					++usedArgs;
				}
				else
				{
					for(size_t restWidthDigits = maxWidthDigits; fmt[0].isDigit(); --restWidthDigits)
					{
						assert(restWidthDigits); // Too many format specifier width digits.
						width = width * 10 + (fmt[0] - '0');
						fmt = fmt[1 .. $];
						assert(!fmt.empty); // Unexpected end of format string after format specifier width.
					}
				}

				const type = fmt[0];
				assert(type.among('b', 'd', 'x', 'X')); // Invalid format specifier type '?'.", fmt.frontSequence));
				assert(!plus || type == 'd'); // Format specifier plus can only be used with 'd' specifier type, not '?'.
				assert(!leadingZero || type != 'd'); // Format specifier leading zero can't be used with 'd' specifier type.

				assert(usedArgs < args.length); // More arguments needed.
				assert(argTypes[usedArgs] != _ArgumentType.other); // Format specifier type '?' can only be used with integral values.
				assert(type == 'd' || argTypes[usedArgs] == _ArgumentType.unsignedInteger); // Format specifier type '?' can only be used with unsigned integral values.
				fmt = fmt[1 .. $];

				putArg(usedArgs, FormatSpec(type, plus, leadingZero, width));
				++usedArgs;

				break;
		}
	}

	assert(usedArgs == args.length); // Redundant arguments.
}


struct FormatSpec
{
	char type = '*';
	bool plus = false;
	bool leadingZero = false;
	size_t width = 0;
}

void formatValue(Char, Writer, T)(Writer w, in FormatSpec spec, auto ref T val)
if(isOutputRange!(Writer, Char) && isSomeChar!Char)
{
	// FIXME: Used `std.format.formatValue` is neither `nothrow` nor `@nogc`.
	// As a result this function isn't `@nogc` even if `Writer` is.
	import std.exception: assumeWontThrow;
	static import std.format;

	static if(is(T == enum))
	{
		static assert(0, "Can't format enum '" ~ T.stringof ~ "'.");
	}
	else static if(isIntegral!T || isSomeChar!T || isSomeString!T)
	{
		if(spec.plus)
		{
			static if(isIntegral!T)
			{
				if(val > 0)
					put(w, '+');
			}
			else
				assert(0);
		}

		if(spec.leadingZero)
		{
			static if(isUnsigned!T)
			{
				const binary = spec.type == 'b';
				if(spec.width || val > (binary ? 1 : 9))
					put(w, binary ? "0b" : "0x");
			}
			else
				assert(0);
		}

		std.format.FormatSpec!Char _spec;

		if(spec.type != '*')
		{
			_spec.spec = spec.type;
			assert(isIntegral!T);
		}

		if(spec.width)
		{
			_spec.precision = spec.width;
			assert(isIntegral!T);
		}

		std.format.formatValue(w, val, _spec).assumeWontThrow();
	}
	else static if(is(T == struct) || is(T == class))
	{
		static assert(__traits(compiles, val.toString()) && isSomeString!(typeof(val.toString())),
			"Can't format user type '" ~ T.stringof ~ "' without suitable `toString` method.");

		static if(is(T == class))
		{
			static assert(!is(Unqual!T == Object), "Can't format a class instance of static type 'Object'."
				~ " The static type has to overload `toString`.");

			static assert(&T.toString != &Object.toString, "Can't format a class instance of static type '"
				~ T.stringof ~ "' which doesn't overload `toString`.");

			assert(val, "Can't format `null` of class '" ~ T.stringof ~ "'.");
		}
		std.format.FormatSpec!Char _spec;
		std.format.formatValue(w, val.toString(), _spec).assumeWontThrow();
	}
	else
		static assert(0, "Can only format integral, character, or string value, not '" ~ T.stringof ~ "'.");
}


enum maxWidthDigits = 2;

// Note: `other` type is also used for invalid types.
private enum _ArgumentType { signedInteger, unsignedInteger, other }

private _ArgumentType[] _getArgumentTypes(A...)() @safe pure nothrow
{
	auto res = new _ArgumentType[A.length];
	foreach(const i, T; A)
	{
		static if(!is(T == enum) && isIntegral!T)
			res[i] = isSigned!T ? _ArgumentType.signedInteger : _ArgumentType.unsignedInteger;
		else
			res[i] = _ArgumentType.other;
	}
	return res;
}

template isFormatValid(A...)
{
	static immutable argTypes = _getArgumentTypes!A();

	bool isFormatValid(Char)(in Char[] format) @safe pure nothrow @nogc
	if(isSomeChar!Char)
	{
		return isFormatValidImpl(format, argTypes);
	}
}

private bool isFormatValidImpl(Char)(in Char[] format, in _ArgumentType[] args) @safe pure nothrow @nogc
if(isSomeChar!Char)
{
	size_t usedArgs = 0;
	bool escaped = false;

	auto fmt = format[];
	L: for(;;)
	{
		const(Char)[] rawChunk;
		const n = fmt._skipFmtRawChunk(escaped, rawChunk);
		final switch(n)
		{
			case 0: // not found
				if(fmt.empty)
					break L;
				break;
			case 1: // '*'
				if(usedArgs == args.length)
					return false; // More arguments needed.
				++usedArgs;
				break;
			case 2: // '^'
				if(fmt.empty)
					return false; // Start of format specifier '^' at end of string.

				if(fmt[0] == '^') // '^^'
					break;

				if(fmt[0] == '*')
				{
					if(fmt.length < 2)
						return false; // Unfinished format specifier '^*'.

					if(fmt[1] == '^') // '^*^'
						break;
				}

				escaped = false;

				const plus = fmt[0] == '+';
				if(plus)
				{
					fmt = fmt[1 .. $];
					if(fmt.empty)
						return false; // Unexpected end of format string after format specifier plus.
				}

				const leadingZero = fmt[0] == '0';
				if(leadingZero)
				{
					fmt = fmt[1 .. $];
					if(fmt.empty)
						return false; // Unexpected end of format string after format specifier leading zero.
					if(fmt[0] == '0')
						return false; // Format specifier width can't start with '0'.
				}

				if(fmt[0] == '*') // argument dependent width
				{
					fmt = fmt[1 .. $];
					if(fmt.empty)
						return false; // Unexpected end of format string after format specifier argument dependent width.

					if(usedArgs == args.length)
						return false; // More arguments needed.

					if(args[usedArgs] == _ArgumentType.other)
						return false; // Format specifier argument dependent width can only be used with integral values.

					++usedArgs;
				}
				else for(size_t restWidthDigits = maxWidthDigits; fmt[0].isDigit(); --restWidthDigits)
				{
					if(!restWidthDigits)
						return false; // Too many format specifier width digits.
					fmt = fmt[1 .. $];
					if(fmt.empty)
						return false; // Unexpected end of format string after format specifier width.
				}

				const type = fmt[0];
				if(!type.among('b', 'd', 'x', 'X'))
					return false; // Invalid format specifier type '?'.

				if(plus && type != 'd')
					return false; // Format specifier plus can only be used with 'd' specifier type, not '?'.
				if(leadingZero && type == 'd')
					return false; // Format specifier leading zero can't be used with 'd' specifier type.

				if(usedArgs == args.length)
					return false; // More arguments needed.

				if(args[usedArgs] == _ArgumentType.other)
					return false; // Format specifier type '?' can only be used with integral values.

				if(type != 'd' && args[usedArgs] != _ArgumentType.unsignedInteger)
					return false; // Format specifier type '?' can only be used with unsigned integral values.

				fmt = fmt[1 .. $];

				++usedArgs;

				break;
		}
	}

	if(usedArgs < args.length)
		return false; // Redundant arguments.

	return true;
}

template assumeFormatValid(A...)
{
	static immutable argTypes = _getArgumentTypes!A();

	void assumeFormatValid(Char)(in Char[] format) @safe pure nothrow @nogc
	if(isSomeChar!Char)
	{
		assumeFormatValidImpl(format, argTypes);
	}
}

private void assumeFormatValidImpl(Char)(in Char[] format, in _ArgumentType[] args) @safe pure nothrow @nogc
if(isSomeChar!Char)
{
	Char[73 + 4 - 1] sbuff = void;
	const(Char)[] formatError(in Char[] errorFmt, in Char[] str) @safe pure nothrow @nogc
	{
		return sbuff._simpleFormat('?', errorFmt, str);
	}

	size_t usedArgs = 0;
	bool escaped = false;

	auto fmt = format[];
	L: for(;;)
	{
		const(Char)[] rawChunk;
		const n = fmt._skipFmtRawChunk(escaped, rawChunk);
		final switch(n)
		{
			case 0: // not found
				if(fmt.empty)
					break L;
				break;
			case 1: // '*'
				assert(usedArgs < args.length, "More arguments needed.");
				++usedArgs;
				break;
			case 2: // '^'
				assert(!fmt.empty, "Start of format specifier '^' at end of string.");

				if(fmt[0] == '^') // '^^'
					break;

				if(fmt[0] == '*')
				{
					assert(fmt.length > 1, "Unfinished format specifier '^*'.");
					if(fmt[1] == '^') // '^*^'
						break;
				}

				escaped = false;

				const plus = fmt[0] == '+';
				if(plus)
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty, "Unexpected end of format string after format specifier plus.");
				}

				const leadingZero = fmt[0] == '0';
				if(leadingZero)
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty, "Unexpected end of format string after format specifier leading zero.");
					assert(fmt[0] != '0', "Format specifier width can't start with '0'.");
				}

				const hasArgWidth = fmt[0] == '*';
				if(hasArgWidth) // argument dependent width
				{
					fmt = fmt[1 .. $];
					assert(!fmt.empty, "Unexpected end of format string after format specifier argument dependent width.");

					assert(usedArgs < args.length, "More arguments needed.");
					// Note: argument type will be checked later for better error message.
					++usedArgs;

					// Note: additional check for better error message.
					assert(!fmt[0].isDigit(), "Both format specifier argument dependent width and width can't be present.");
				}
				else
				{
					for(size_t restWidthDigits = maxWidthDigits; fmt[0].isDigit(); --restWidthDigits)
					{
						assert(restWidthDigits, "Too many format specifier width digits.");
						fmt = fmt[1 .. $];
						assert(!fmt.empty, "Unexpected end of format string after format specifier width.");
					}

					// Note: additional check for better error message.
					assert(fmt[0] != '*', "Both format specifier width and argument dependent width can't be present.");
				}

				const type = fmt[0];
				assert(type.among('b', 'd', 'x', 'X'),
					formatError("Invalid format specifier type '?'.", fmt.frontSequence));

				assert(!plus || type == 'd',
					formatError("Format specifier plus can only be used with 'd' specifier type, not '?'.", fmt[0 .. 1]));

				assert(!leadingZero || type != 'd',
					"Format specifier leading zero can't be used with 'd' specifier type.");


				if(hasArgWidth) // argument dependent width
				{
					assert(args[usedArgs - 1] != _ArgumentType.other,
						"Format specifier argument dependent width can only be used with integral values.");
				}

				assert(usedArgs < args.length, "More arguments needed.");

				assert(args[usedArgs] != _ArgumentType.other,
					formatError("Format specifier type '?' can only be used with integral values.", fmt[0 .. 1]));

				assert(type == 'd' || args[usedArgs] == _ArgumentType.unsignedInteger,
					formatError("Format specifier type '?' can only be used with unsigned integral values.", fmt[0 .. 1]));

				fmt = fmt[1 .. $];

				++usedArgs;

				break;
		}
	}

	assert(usedArgs == args.length, "Redundant arguments.");
}

@safe pure nothrow unittest
{
	import std.typetuple : TypeTuple;

	foreach(const i, const fmt0; ["", "*", "**", "^*^", "^^"])
	{
		const args = i > 2 ? [] : [_ArgumentType.other, _ArgumentType.other][0 .. i];
		foreach(const fmt; [fmt0, "a" ~ fmt0, fmt0 ~ "a"])
		{
			assert(isFormatValidImpl(fmt, args));
			if(!args.empty)
				assert(!isFormatValidImpl(fmt, args[0 .. $ - 1]));
			assert(!isFormatValidImpl(fmt, args ~ _ArgumentType.other));
			assumeFormatValidImpl(fmt, args);
		}
	}

	foreach(const fmt0; ["^", "^?", "^*", "^*?"])
	{
		foreach(const fmt; [fmt0, "a" ~ fmt0, fmt0 ~ "a"])
			assert(!isFormatValidImpl(fmt, []));
	}

	foreach(const fmt0; ["^", "^0", "^2", "^02", "^035"])
	{
		foreach(const type; "bdxX?")
		{
			const fmt1 = fmt0 ~ type;
			foreach(const fmt; [fmt1, "a" ~ fmt1, fmt1 ~ "a"])
			{
				if(type == '?' || type == 'd' && fmt1[1] == '0')
				{
					assert(!isFormatValid!uint(fmt));
					continue;
				}
				assert(isFormatValid!uint(fmt));
				assumeFormatValid!uint(fmt);

				const supportSigned = type == 'd';
				assert(isFormatValid!int(fmt) == supportSigned);
				if(supportSigned)
					assumeFormatValid!int(fmt);

				assert(!isFormatValid(fmt));
				assert(!isFormatValid!string(fmt));
				assert(!isFormatValid!(uint, uint)(fmt));
			}
		}
	}

	assert(!isFormatValid!uint("^00d"));
	foreach(const s1; ["111", "1*", "*1"])
	{
		foreach(const s2; ["^" ~ s1, "^0" ~ s1])
		{
			foreach(const fmt; [s2, s2 ~ 'd', s2 ~ 'x'])
			{
				assert(!isFormatValid!uint(fmt));
				assert(!isFormatValid!(uint, uint)(fmt));
			}
		}
	}

	foreach(const s2; ["^+d", "^+0d"])
	{
		foreach(T; TypeTuple!(byte, ubyte, int, uint))
		{
			assert(isFormatValid!T("^+d"));
			assumeFormatValid!T("^+d");
		}
	}
	foreach(const fmt; ["^+b", "^+x", "^+X"])
		assert(!isFormatValid!uint(fmt));
}

private:

const(Char)[] _simpleFormat(Char)(Char[] buff, in char ch, in Char[] errorFmt, in Char[] arg) @safe pure nothrow @nogc
if(isSomeChar!Char)
{
	import std.string: representation;

	auto res = buff[0 .. errorFmt.length + arg.length - 1];
	const i = errorFmt.representation().countUntil(ch);
	res[0 .. i] = errorFmt[0 .. i];
	res[i .. i + arg.length] = arg;
	res[i + arg.length .. $] = errorFmt[i + 1 .. $];
	return res;
}

size_t _skipFmtRawChunk(Char)(ref const(Char)[] fmt, ref bool escaped, out const(Char)[] chunk) @safe pure nothrow @nogc
if(isSomeChar!Char)
{
	import std.string: representation;

	if(escaped && fmt[0] == '*') // ^*^
	{
		escaped = false;
		chunk = fmt[0 .. 1];
		fmt = fmt[2 .. $];
		return 0;
	}

	auto r = fmt[escaped .. $].representation().find('*', '^');

	escaped = r[1] == 2; // '^'
	chunk = fmt[0 .. $ - r[0].length];
	fmt = cast(const Char[]) r[0][!r[0].empty .. $];

	return r[1];
}
