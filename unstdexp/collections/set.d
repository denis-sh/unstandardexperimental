﻿/**
Set implementation.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.collections.set;


struct Set(T)
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		bool[T] _aa = null;
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property
	{
		size_t length() const pure nothrow @nogc
		{ return _aa.length; }

		bool empty() const pure nothrow @nogc
		{ return !_aa.length; }

		inout(T)[] dup() inout
		{ return (/* FIXME: AA @@@BUG@@@ workaround */() @trusted => _aa.keys)(); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	void opOpAssign(string op : "+")(T t)
	in { assert(t !in this); }
	body
	{
		_aa[t] = true;
	}

	void opOpAssign(string op : "-")(in T t)
	in { assert(t in this); }
	body
	{
		_aa.remove(t);
	}

	void opOpAssign(string op : "|")(T t)
	{
		_aa[t] = true;
	}

	bool opBinaryRight(string op : "in")(in T t) const
	{
		return !!(t in _aa);
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	void clear()
	{
		_aa = null;
	}
}

Set!T set(T)()
{
	return Set!T();
}

@safe pure nothrow unittest
{
	import std.algorithm: sort;

	T[] sorted(T)(T[] arr) @trusted
	{ return arr.sort().release(); }

	auto set = set!int();

	assert(1 !in set && set.empty);

	set += 1;
	set += 2;
	set |= 2;
	set += 3;
	assert(1 in set && 2 in set);
	assert(sorted(set.dup) == [1, 2, 3]);

	set -= 2;
	assert(sorted(set.dup) == [1, 3]);
	assert(1 in set && 2 !in set);

	set -= 3;
	set -= 1;
	assert(1 !in set && set.empty);
}
