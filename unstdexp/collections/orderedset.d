﻿/**
Ordered set implementation.

Copyright: Denis Shelomovskij 2013-2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.collections.orderedset;


import std.algorithm;
import std.exception; // for `assumeWontThrow`
import std.functional; // for `binaryFun`
import std.range;

import unstdexp.utils; // for `inoutDup`


// FIXME: The implementation is very slow. It works internaly with
// the single array so all operations have linear time.
struct OrderedSet(T, alias pred)
if(is(typeof(binaryFun!pred)))
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		T[] _buff;
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property
	{
		size_t length() const pure nothrow @nogc
		{ return _buff.length; }

		bool empty() const pure nothrow @nogc
		{ return !_buff.length; }

		inout(T)[] buff() inout pure nothrow @nogc
		{ return _buff; }

		inout(T)[] dup() inout
		{ return _buff.inoutDup; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	void opOpAssign(string op : "~")(T t)
    { append(t); }

	void opOpAssign(string op : "~")(T[] arr)
    { appendAll(arr); }

	bool opBinaryRight(string op : "in")(in T t) const
    { return contains(t); }

	// Functions
	// ----------------------------------------------------------------------------------------------------

	bool contains(in T t) const
	{
		return _buff.canFind!pred(t);
	}

	size_t indexOf(in T t) const
	in { assert(contains(t)); }
	body
	{
		return _buff.countUntil!pred(t);
	}

	void insertAt(in size_t pos, T t)
	in { assert(!contains(t)); }
	body
	{
		_buff.insertInPlace(pos, t);
	}

	void append(T t)
	in { assert(!contains(t)); }
	body
	{
		_buff ~= t;
	}

	void appendAll(T[] arr...)
	in { assert(canAdd(arr)); }
	body
	{
		_buff ~= arr;
	}

	bool tryAppend(T t)
	{
		if(contains(t))
			return false;
		append(t);
		return true;
	}

	void insertAtOrAppend(in size_t pos, T t)
	in { assert(!contains(t)); }
	body
	{
		if(pos == -1)
			append(t);
		else
			insertAt(pos, t);
	}

	size_t remove(in T t)
	in { assert(contains(t)); }
	body
	{
		const idx = indexOf(t);
		_buff = _buff._removeFix(idx);
		return idx;
	}

	void removeAll(in T[] arr...)
	in { assert(arr.all!(t => contains(t))()); }
	body
	{
		foreach(const t; arr)
			remove(t);
	}

	T removeAt(in size_t pos)
	in { assert(pos < length); }
	body
	{
		T res = _buff[pos];
		_buff = _buff._removeFix(pos);
		return res;
	}

	T removeLast()
	{ return removeAt(length - 1); }

	void copyFrom(R)(R r)
	if(isInputRange!R && is(ElementType!R == T))
	out
	{
		assert(isUnique(_buff));
	}
	body
	{
		_buff = r.array();
	}

	T[] releaseBuff() pure nothrow @nogc
	{
		auto res = _buff;
		_buff = null;
		return res;
	}

	void clear() pure nothrow @nogc
	{
		_buff = null;
	}

	bool canAdd(T[] arr...)
	{
		return isUnique(arr) && arr.all!(t => !contains(t))();
	}

	static bool isUnique(in T[] arr...)
	{
		if(!arr.empty)
		{
			foreach(const i, ref t; arr[0 .. $ - 1])
			{
				if(arr[i + 1 .. $].canFind!pred(t))
					return false;
			}
		}
		return true;
	}
}

template OrderedObjectSet(T)
if(is(T == class) || is(T == interface))
{
	alias OrderedObjectSet = OrderedSet!(T, `a is b`);
}

@safe pure nothrow unittest
{
	OrderedSet!(int, `a == b`) oset;

	assert(1 !in oset && oset.empty);

	oset ~= 1;
	oset ~= [2, 3];
	assert(1 in oset && 2 in oset);
	assert(oset.buff == [1, 2, 3]);

	assert(!oset.canAdd(1));
	assert( oset.canAdd(4, 5, 6));
	assert(!oset.canAdd(4, 5, 4));

	oset.remove(2);
	assert(oset.buff == [1, 3]);
	assert(1 in oset && 2 !in oset);

	oset.removeAll(3, 1);
	assert(1 !in oset && oset.empty);
}

// NOTE: workaround the fact `remove` is `@system` and not `nothrow`.
private T[] _removeFix(T)(T[] arr, in size_t idx) @trusted
{
	return arr.remove(idx).assumeWontThrow();
}
