﻿/**
Decimal fraction storage implementation.

Copyright: Denis Shelomovskij 2013-2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.decimalstore;


import std.math;
import std.traits;

import unstdexp.format;


@safe pure:

struct DecimalStore
{
pure:

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum
	{
		base = 10u,
		maxDigits = 18u,
		maxAbsData = (cast(long) base ^^ cast(long) maxDigits) - 1,
	}

	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		uint _fracDigits;
		long _data, _dataK;
		real _realValue;
	}

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	@disable this();

	this(in long integral) nothrow @nogc
	{ this(0, integral, 0); }

	this(in uint fracDigits, in long integralPart, in long fractionPart = 0) nothrow @nogc
	in
	{
		assert(fracDigits <= maxDigits);
		assert(fractionPart < (cast(long) base) ^^ cast(long) fracDigits);
	}
	body
	{
		this = fromData(fracDigits, fractionPart);
		_data += integralPart * _dataK;
		_realValue += integralPart;
	}

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static DecimalStore fromData(in uint fracDigits, in long data) nothrow @nogc
	in { assert(fracDigits <= maxDigits && abs(data) <= maxAbsData); }
	body
	{
		auto res = DecimalStore.init;
		res._fracDigits = fracDigits;
		res._data = data;
		const dataK = res._dataK = (cast(long) base) ^^ cast(long) fracDigits;
		res._realValue = cast(real) data / dataK;
		return res;
	}

	static DecimalStore truncateFrom(in uint fracDigits, in DecimalStore from) nothrow @nogc
	in
	{
		assert(fracDigits <= maxDigits);
		const d = cast(long) fracDigits - cast(long) from.fracDigits;
		if(d > 0)
			assert(from.data <= maxAbsData / (cast(long) base) ^^ d);
	}
	body
	{
		long data = from.data;
		const d = cast(long) fracDigits - cast(long) from.fracDigits;
		const k = (cast(long) base) ^^ abs(d);
		if(d > 0)
			data *= k;
		else if(d < 0)
			data /= k;
		return fromData(fracDigits, data);
	}

	static DecimalStore roundFrom(in uint fracDigits, in DecimalStore from) nothrow @nogc
	in
	{
		assert(fracDigits <= maxDigits);
		const dFracDigits = cast(long) fracDigits - cast(long) from.fracDigits;
		if(dFracDigits > 0)
			assert(from.data <= maxAbsData / (cast(long) base) ^^ dFracDigits);
	}
	body
	{
		long data = from.data;
		const dFracDigits = cast(long) fracDigits - cast(long) from.fracDigits;
		const k = (cast(long) base) ^^ abs(dFracDigits);
		if(dFracDigits > 0)
		{
			data *= k;
		}
		else if(dFracDigits < 0)
		{
			const fracAbsData = abs(data) % k;
			data /= k;
			if(fracAbsData)
			{
				const k_base = k / cast(long) base;
				const d = cast(int) (fracAbsData / k_base) - 5;
				const plusOne = d > 0
					|| !d && (fracAbsData % k_base)
					|| !d && (data % 2);
				if(plusOne)
					from.data > 0 ? ++data : --data;
			}
		}

		return fromData(fracDigits, data);
	}

	static DecimalStore roundFrom(in uint fracDigits, in real floating) nothrow @nogc
	in { assert(fracDigits <= maxDigits && !isNaN(floating)); }
	body
	{ return DecimalStore.fromData(fracDigits, _lround(floating * (cast(long) base) ^^ cast(long) fracDigits)); }

	private static size_t _dummySize;

	static DecimalStore roundFrom(S)(in S str, out size_t originFracDigits = _dummySize)
	if(isSomeString!S)
	{
		return roundFrom(0, maxDigits, str, originFracDigits);
	}

	static DecimalStore roundFrom(S)(in uint fracDigits, in S str, out size_t originFracDigits = _dummySize)
	if(isSomeString!S)
	{
		return roundFrom(fracDigits, fracDigits, str, originFracDigits);
	}

	static DecimalStore roundFrom(S)(in uint minFracDigits, in uint maxFracDigits, in S str, out size_t originFracDigits = _dummySize)
	if(isSomeString!S)
	in { assert(minFracDigits <= maxFracDigits && maxFracDigits <= maxDigits); }
	body
	{
		import std.algorithm;
		static import std.ascii;
		import std.conv;
		import std.exception : assumeWontThrow;
		import std.range;
		import std.string : leftJustify;

		const r = str.findSplit(".");
		const neg = r[0].startsWith('-');
		const uns = r[0][neg .. $];
		const frac = r[2];
		if(uns.empty && frac.empty)
			throw new ConvException("Unexpected end of input.");
		if(!chain(uns, frac).all!(std.ascii.isDigit)())
			throw new ConvException(format("Fixed decimal conversion error for input \"*\".", str));

		const size_t originIntDigits = uns.find!`a != '0'`().length;
		originFracDigits = frac.length;

		uint fracDigits = (maxDigits > originIntDigits ?
				min(frac.length, maxDigits - cast(uint) originIntDigits) : 0u)
			.max(minFracDigits)
			.min(maxFracDigits);

		const roundingAddsDigit = frac.length > fracDigits // rounding needed
			&& (originIntDigits || fracDigits)
			&& frac[fracDigits] >= '5'
			&& chain(uns[$ - originIntDigits .. $], frac[0 .. fracDigits]).all!`a == '9'`();

		size_t totalDigits = originIntDigits + fracDigits;

		if(roundingAddsDigit)
		{
			if(totalDigits >= maxDigits && fracDigits > minFracDigits)
				--fracDigits;
			else
				++totalDigits;
		}

		if(totalDigits > maxDigits)
		{
			throw new ConvOverflowException(format("Fixed decimal can't hold * digits"
				~ " (including * for fractional part), at most * digits allowed.",
					totalDigits, fracDigits, maxDigits));
		}

		if(roundingAddsDigit)
		{
			const absData = cast(long) base ^^ cast(long) (totalDigits - 1);
			return DecimalStore.fromData(fracDigits, neg ? -absData : absData);
		}

		const d = frac.length > fracDigits ? cast(char) frac[fracDigits] - '5' : -5;
		const plusOne = d > 0
			|| !d && frac[fracDigits + 1 .. $].any!`a != '0'`()
			|| !d && ((fracDigits ? frac[fracDigits - 1] : uns[$ - 1]) % 2); // Note: '0' has even value.

		const truncatedData = text(r[0], frac[0 .. min($, fracDigits)].leftJustify(fracDigits, '0'))
			.to!long().assumeWontThrow();

		const data = truncatedData + (plusOne ? neg ? -1 : 1 : 0);

		return DecimalStore.fromData(fracDigits, data);
	}

	static DecimalStore parseRoundFrom(S)(in uint fracDigits, ref S str)
	if(isSomeString!S && !is(S == enum))
	in { assert(fracDigits <= maxDigits); }
	body
	{
		static import std.ascii;

		size_t i = 0;
		for(int j = 0; j < 2; ++j)
		{
			if(!j && (i < str.length && str[i] == '-'))
				++i;
			while(i < str.length && std.ascii.isDigit(str[i]))
				++i;
			if(!j)
			{
				if(i < str.length && str[i] == '.')
					++i;
				else
					break;
			}
		}
		const res = DecimalStore.roundFrom(fracDigits, str[0 .. i]);
		str = str[i .. $];
		return res;
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property const nothrow @nogc
	{
		uint fracDigits()
		{ return _fracDigits; }

		uint actualFracDigits()
		{
			uint d = fracDigits;
			for(long data = data; d && !(data % base); --d)
				data /= base;
			return d;
		}

		real realValue()
		{ return _realValue; }

		long integralValue()
		in { assert(!fracDigits); }
		body
		{ return data; }

		long truncated()
		{ return data / _dataK; }

		long data()
		{ return _data; }

		long fractionData()
		{ return data % _dataK; }

		DecimalStore rounded()
		{ return roundFrom(actualFracDigits, this); }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const nothrow @nogc
	{
		bool opCast(T : bool)()
		{
			return !!data;
		}

		DecimalStore opUnary(string op : "-")()
		{ return fromData(fracDigits, -data); }

		DecimalStore opUnary(string op : "+")()
		{ return this; }

		int opCmp(in DecimalStore rhs)
		in { assert(fracDigits == rhs.fracDigits); } // TODO not needed?
		body{
			return data < rhs.data ? -1 : (data == rhs.data ? 0 : 1);
		}
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	string toString() const nothrow
	{
		if(!fracDigits)
			return format("*", data);
		const t = truncated;
		return format("**.^*d", data < 0 ? "-" : "", abs(t), fracDigits, abs(data - t * _dataK));
	}
}

nothrow @nogc unittest
{
	alias Dec = DecimalStore;

	assert(!Dec.fromData(2, 0));
	assert( Dec.fromData(2, 1));

	assert(Dec.fromData(2, 1) == +Dec.fromData(2, 1));
	assert(Dec.fromData(2, 1) == -Dec.fromData(2, -1));
}

nothrow unittest
{
	alias Dec = DecimalStore;

	static assert(Dec.maxDigits == 18);
	assert(Dec.fromData(0, -Dec.maxAbsData).toString() == "-999999999999999999");
	assert(Dec.fromData(0,  Dec.maxAbsData).toString() ==  "999999999999999999");
	assert(Dec.fromData(Dec.maxDigits, -Dec.maxAbsData).toString() == "-0.999999999999999999");
	assert(Dec.fromData(Dec.maxDigits,  Dec.maxAbsData).toString() ==  "0.999999999999999999");

	assert(Dec.fromData(0, 17).toString() == "17");
	assert(Dec.fromData(1, 17).toString() == "1.7");
	assert(Dec.fromData(2, 17).toString() == "0.17");
	assert(Dec.fromData(2, 0).toString() == "0.00");
	assert(Dec.fromData(2, 300).toString() == "3.00");
	assert(Dec(0, 17, 0).toString() == "17");
	assert(Dec(1, 1, 7).toString() == "1.7");
	assert(Dec(2, 0, 17).toString() == "0.17");

	assert(Dec.truncateFrom(2, Dec.fromData(0, 17)).toString() == "17.00");
	assert(Dec.truncateFrom(0, Dec.fromData(2, 1789)).toString() == "17");
	assert(Dec.truncateFrom(1, Dec.fromData(2, -1789)).toString() == "-17.8");
}

unittest
{
	alias Dec = DecimalStore;

	assert(Dec(1).realValue == 1);

	size_t d = -1;
	assert(Dec.roundFrom("1.23456789012345000000000", d).toString() ==  "1.23456789012345000" && d == 23);
	assert(Dec.roundFrom(".123456789012345000000000", d).toString() == "0.123456789012345000" && d == 24);
	foreach(const prefix; ["", "0", "00", "000"])
	{
		foreach(const suffix; ["", "4", "49", "499"])
		{
			const str = prefix ~ ".999999999999999999" ~ suffix;
			const strNeg = '-' ~ str;
			const n = Dec.maxDigits + suffix.length;
			assert(Dec.roundFrom(str, d).toString()    ==  "0.999999999999999999" && d == n);
			assert(Dec.roundFrom(strNeg, d).toString() == "-0.999999999999999999" && d == n);
			assert(Dec.roundFrom(5, 17, str, d).toString()    ==  "1.00000000000000000" && d == n);
			assert(Dec.roundFrom(0, 16, strNeg, d).toString() == "-1.0000000000000000" && d == n);
		}
	}

	foreach(const from, const to; [
		"0": 0, "0.49": 0, "0.5": 0, "0.50": 0, "0.51": 1,
		"1": 1, "1.49": 1, "1.5": 2, "1.50": 2, "1.51": 2,
		"2": 2, "2.49": 2, "2.5": 2, "2.50": 2, "2.51": 3,
		"3.": 3,
	])
	{
		assert(Dec.roundFrom(0, from, d).integralValue == to);
		assert(Dec.roundFrom(0, '-' ~ from).integralValue == -to);

		const n = Dec.roundFrom(from);
		const nNeg = Dec.roundFrom('-' ~ from);
		assert(Dec.roundFrom(0, n).integralValue == to);
		assert(Dec.roundFrom(0, nNeg).integralValue == -to);

		const roundedFrom = from[0 .. $ - (from[$ - 1] == '.')];
		assert(n.toString() == roundedFrom);
		assert(nNeg.toString() == (roundedFrom == "0" ? "0" : '-' ~ roundedFrom));
	}

	foreach(const from, const to; [
		".1": "0.1", ".149": "0.1", ".15": "0.2", ".150": "0.2", ".151": "0.2",
		".2": "0.2", ".249": "0.2", ".25": "0.2", ".250": "0.2", ".251": "0.3",
		"1": "1.0",
		"7.8500000000000000000000000000000000000000000000000000": "7.8",
		"7.8500000000000000000000000000000000000000000000000001": "7.9",
		"99999999999999999.94": "99999999999999999.9",
	])
	{
		import std.algorithm : min;

		assert(Dec.roundFrom(1, from).toString() == to);
		assert(Dec.roundFrom(1, '-' ~ from).toString() == '-' ~ to);

		const roundedFrom = (from[0] == '.' ? '0' ~ from : from)
			[0 .. min($, Dec.maxDigits + 1)];
		assert(Dec.roundFrom(from).toString() == roundedFrom);
		assert(Dec.roundFrom('-' ~ from).toString() == '-' ~ roundedFrom);
	}

	foreach(const fracStr, const actualFracDigits; [
		"": 0, ".0": 0, ".00": 0, ".10": 1, ".900": 1,
		".01": 2, ".11": 2, ".99": 2, ".99000": 2,
		".99001": 5, ".10001000000": 5,
	])
	{
		foreach(const n; ["0", "1", "-1", "500", "99999", "-50"])
		{
			const decStr = n ~ fracStr;
			const decRoundedStr = actualFracDigits ?
				n ~ fracStr[0 .. actualFracDigits + 1] : n;

			const d = Dec.roundFrom(decStr);
			assert(d.actualFracDigits == actualFracDigits);
			assert(d.rounded.toString() == decRoundedStr);
		}
	}

	{
		string str = "-.230.ab";
		assert(Dec.parseRoundFrom(3, str).toString() == "-0.230");
		assert(str == ".ab");
	}
}

private long _lround(in real x) @trusted nothrow @nogc
{
	// FIXME lround not implemented, use real.to!long ?

	// Note: We use this hack to treat `round` as `pure`.
	enum pureRound = cast(real function(real) @trusted pure nothrow @nogc) &round;

	return cast(long) pureRound(x);
}
