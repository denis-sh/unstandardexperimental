﻿/**
General utility functions.

Copyright: Denis Shelomovskij 2013-2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.utils;


import std.functional; // for `binaryFun`
import std.traits; // for `isImplicitlyConvertible`, `isUnsigned`


// Value updating function:
// ----------------------------------------------------------------------------------------------------

bool tryUpdate(alias pred = `a == b`, T)(ref T var, T newValue)
if(is(typeof(binaryFun!pred)))
{
	if(binaryFun!pred(var, newValue))
		return false;
	var = newValue;
	return true;
}

bool tryUpdate(alias pred = `a == b`, T, U)(ref T var, U newValue)
if(is(typeof(binaryFun!pred)) && !is(U == T) && !isImplicitlyConvertible!(U, T))
{
	// This code is just for 'cannot implicitly convert expression'
	// compiler error:
	var = newValue;
}

bool tryUpdate(alias pred = `a == b`, T)(ref T var, in bool varDefined, T newValue)
if(is(typeof(binaryFun!pred)))
{
	if(varDefined && binaryFun!pred(var, newValue))
		return false;
	var = newValue;
	return true;
}

bool tryUpdate(alias pred = `a == b`, T, U)(ref T var, in bool varDefined, U newValue)
if(is(typeof(binaryFun!pred)) && !is(U == T) && !isImplicitlyConvertible!(U, T))
{
	// This code is just for 'cannot implicitly convert expression'
	// compiler error:
	var = newValue;
}

@safe pure nothrow @nogc unittest
{
	int i = 1;
	assert(!i.tryUpdate(1));
	assert(!i.tryUpdate(true, 1));
	assert(i.tryUpdate(false, 1));
	assert(i.tryUpdate(cast(const int) 3) && i == 3);
}


// Value inverting functions:
// ----------------------------------------------------------------------------------------------------

void invert(ref bool val) @safe pure nothrow @nogc
{ val = !val; }

void invert(T)(ref T val)
{
	static assert(!isUnsigned!T, "`invert`: Can't invert unsigned type '" ~ T.stringof ~ "'.");
	val = -val;
}

// Note: we have to use specialized template because of dmd @@@BUG9999@@@.
bool invertedIf(T : bool)(in T val, in bool invert) @safe pure nothrow @nogc
{ return val ^ invert; }

T invertedIf(T)(in T val, in bool invert)
{
	static assert(!isUnsigned!T, "`invertedIf`: Can't invert unsigned type '" ~ T.stringof ~ "'.");
	return invert ? -val : val;
}

@safe pure nothrow @nogc unittest
{
	int i = 1;
	i.invert();
	assert(i == -1);

	alias Seq(expr...) = expr;
	foreach(val; Seq!(true, false, cast(byte) 1, 1, 3.4))
		assert(val.invertedIf(false) == val);

	foreach(T; Seq!(ubyte, uint, ulong))
	{
		static assert(!__traits(compiles, { T u; u.invert(); }));
		static assert(!__traits(compiles, { T u; u.invertedIf(true); }));
	}

	foreach(u; Seq!(cast(ubyte) 0, 1U, 2UL))
	{
		static assert(!__traits(compiles, { u.invertedIf(true); }));
	}

	assert(!true.invertedIf(true));
	assert(false.invertedIf(true));
	assert(2.invertedIf(true) == -2);
	assert(3.4.invertedIf(true) == -3.4);
}


// `inout` array duplicating function:
// ----------------------------------------------------------------------------------------------------

@property inout(T)[] inoutDup(T)(inout(T)[] arr)
{
	return arr
		._trustedCast!(T[])()
		.dup
		._trustedCast!(inout T[])();
}

private T _trustedCast(T, U)(U val, inout int = 0) @trusted
{ return cast(T) val; }

@safe pure nothrow unittest
{
	inout(int)[] f(inout(int)[] arr) nothrow
	{ return arr.inoutDup; }

	assert(f([1, 2]) == [1, 2]);
}
