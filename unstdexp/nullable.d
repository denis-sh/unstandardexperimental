﻿/**
Simple nullable type implementation.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.nullable;


import std.traits;


struct Nullable(T)
{
	// Static preconditions
	// ----------------------------------------------------------------------------------------------------

	static assert(!is(T == class) && !is(T == interface),
		"Can't construct `Nullable!" ~ T.stringof ~ "`: `" ~ T.stringof ~ "` is already a class or an interface.");
	static assert(!__traits(compiles, { T t = null; }),
		"Can't construct `Nullable!" ~ T.stringof ~ "`: `" ~ T.stringof ~ "` is already constructible from `null`.");
	static assert(!__traits(compiles, { T t = T.init; t = null; }),
		"Can't construct `Nullable!" ~ T.stringof ~ "`: `null` is already assignable to `" ~ T.stringof ~ "`.");

	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		T _value = T.init;
		bool _isNull = true;
	}

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(inout T value) inout
	{
		_value = value;
		_isNull = false;
	}

	this(typeof(null)) inout
	{
		_value = T.init;
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property @safe pure nothrow @nogc
	{
		bool isNull() const
		{
			return _isNull;
		}

		ref inout(T) get() inout 
		in { assert(!isNull, "Called `get` on null `Nullable!" ~ T.stringof ~ "`."); }
		body
		{
			return _value;
		}
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	static if(isAssignable!T)
	{
		ref Nullable opAssign(T value)
		{
			_value = value;
			_isNull = false;
			return this;
		}

		ref Nullable opAssign(typeof(null))
		{
			_value = T.init;
			_isNull = true;
			return this;
		}
	}
}

// Free construction functions
// ----------------------------------------------------------------------------------------------------

@property Nullable!T orNull(T)(T t)
{
	return Nullable!T(t);
}

@property Nullable!T nullOf(T)()
{
	return Nullable!T.init;
}

@safe pure nothrow @nogc unittest
{
	{
		Nullable!int a, b = null, c = 3;
		assert(a.isNull && b.isNull && !c.isNull);
		assert(c.get == 3 && c is 3.orNull);
		assert(4.orNull.get == 4);
		assert(nullOf!int.isNull);
		c = a;
		assert(c.isNull);
		assert((&(c = 5)).get == 5);

		assert((0 ? 6.orNull : nullOf!int).isNull);
		assert((n => !n.isNull && n.get == 7)(1 ? 7.orNull : nullOf!int));
	}
	{
		struct S { int i; int* p; }

		Nullable!S s;
		assert(s.isNull);
		s = S(5);
		assert(!s.isNull && s.get.i == 5);
		s = null;
		assert(s.isNull);

		assert(!(const S()).orNull.isNull);
		assert(!(immutable Nullable!S(immutable S())).isNull);
	}
	{
		struct SNoInit
		{
			@disable this();
			this(int) pure nothrow @nogc { }
		}

		Nullable!SNoInit s;
		assert(s.isNull);
		s = SNoInit(5);
		assert(!s.isNull);
		s = null;
		assert(s.isNull);
	}
	{
		interface I { }

		struct SNullInit
		{
			this(int[]);
		}

		struct SNullAssign
		{
			void opAssign(int[]);
		}

		static assert(!__traits(compiles, Nullable!Object));
		static assert(!__traits(compiles, Nullable!I));
		static assert(!__traits(compiles, Nullable!SNullInit));
		static assert(!__traits(compiles, Nullable!SNullAssign));
	}
}
