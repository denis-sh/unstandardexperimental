﻿/**
Unicode Transformation Format helper functions.

Copyright: Denis Shelomovskij 2015

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstdexp.utf;


import std.traits;

import unstd.utf;


@safe pure nothrow @nogc:

@property inout(Char)[] frontSequence(Char)(inout Char[] str)
if(isSomeChar!Char)
{
	return str[0 .. str[0].stride];
}

@property uint stride(in char c)
in
{
	assert(!c.isContinuationByte(), "Can't get stride from UTF-8 sequence continuation byte.");
}
body
{
    import core.bitop : bsr;

	if(c < 0x80)
		return 1;

    const msbs = 7 - bsr(~c);
    assert(~c && msbs >= 2 && msbs <= 4, "Invalid UTF-8 sequence.");
    return msbs;
}

@property uint stride(in wchar c)
in
{
	assert(!c.isTrailSurrogate(), "Can't get stride from UTF-16 sequence trail surrogate.");
}
body
{
    return 1 + (c >= 0xD800 && c < 0xDC00);
}

@property uint stride(in dchar c)
{
    return 1;
}
